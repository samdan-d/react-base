import { useState } from 'react';

import { Hello } from '@/features/Test';

function App() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <h1 className="text-3xl font-bold text-red-800 underline">Hello world!</h1>
      <Hello />
    </div>
  );
}

export default App;
