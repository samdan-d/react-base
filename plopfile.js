module.exports = (plop) => {
  plop.setGenerator('component', {
    description: 'Create a component',
    // User input prompts provided as arguments to the template
    prompts: [
      {
        type: 'input',
        name: 'folder',
        message: 'What is your folder name?',
      },
      {
        type: 'input',
        name: 'name',
        message: 'What is your component name?',
      },
    ],
    actions: function (data) {
      const { folder, name } = data;
      if (!name) throw new Error('component name is not defined');
      const filePath = `${folder && '{{pascalCase folder}}/'}{{pascalCase name}}`;

      const folderExportActions = folder
        ? [
            {
              type: 'add',
              path: `src/components/{{pascalCase folder}}/index.js`,
              templateFile: 'plop-templates/injectable-index.js.hbs',
              skipIfExists: true,
            },
            {
              type: 'append',
              path: `src/components/{{pascalCase folder}}/index.js`,
              template: `export * from './{{pascalCase name}}';\n`,
            },
          ]
        : [];

      const rootIndexExportName = folder ? '{{pascalCase folder}}' : '{{pascalCase name}}';

      return [
        {
          type: 'add',
          path: `src/components/${filePath}/{{pascalCase name}}.jsx`,
          templateFile: 'plop-templates/components/Component.jsx.hbs',
        },
        {
          type: 'add',
          path: `src/components/${filePath}/index.js`,
          templateFile: 'plop-templates/injectable-index.js.hbs',
          skipIfExists: true,
        },
        {
          type: 'append',
          path: `src/components/${filePath}/index.js`,
          template: `export * from './{{pascalCase name}}';\n`,
        },
        ...folderExportActions,
        {
          type: 'add',
          path: 'src/components/index.js',
          templateFile: 'plop-templates/injectable-index.js.hbs',
          skipIfExists: true,
        },
        {
          type: 'append',
          path: 'src/components/index.js',
          template: `export * from './${rootIndexExportName}';\n`,
        },
      ];
    },
  });
  plop.setGenerator('feature', {
    description: 'Create a feature',
    // User input prompts provided as arguments to the template
    prompts: [
      {
        type: 'input',
        name: 'feature',
        message: 'What is your feature name?',
      },
      {
        type: 'list',
        name: 'type',
        message: 'What is your feature usage?',
        choices: ['api', 'component', 'route'],
      },
    ],
    actions: function (data) {
      const { feature, type } = data;
      if (!feature) throw new Error('feature name is not defined');
    },
  });
};
